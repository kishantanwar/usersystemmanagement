package com.user.system.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.user.system.model.User;

@Repository
@Transactional
public interface UserRepository extends JpaRepository<User, Long>, QuerydslPredicateExecutor<User> {

}
