package com.user.system.service;

import com.user.system.exceptions.ServiceException;
import com.user.system.model.User;

public interface UserService {

	User saveUserDetail(User user)  throws ServiceException;

	User getUserDetailById(Long userId)  throws ServiceException;

	User updateUser(User user)  throws ServiceException, Exception;

	void deleteUsers(Long userId) throws ServiceException;

}
